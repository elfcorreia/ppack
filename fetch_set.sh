#!/bin/bash

function get_pdb {
	wget "https://files.rcsb.org/view/$1.pdb" -o "$1.pdb"
}
export -f get_pdb

function fetch_file {
	cat $1 | grep -v "#" | parallel get_pdb {}
}

function sanitize() {
	# check for modified aminoacids
	# split by dbref
	# split by model
	# split by chain
	echo "sainitizing"
}

fetch_file $1