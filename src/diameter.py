#!/usr/bin/python3

import os, sys, datetime, pwd, argparse, subprocess
from gmmsb.parallel import FileSet, GlobFileSet
from gmmsb.io import read_pdb
from gmmsb.contacts import rel_distance_matrix, distance_matrix, distance_graph, distance_graph2
from matplotlib import pyplot as plt
import networkx as nx
from networkx.algorithms import approximation
from PIL import Image

def diameter(filename):
	sel = read_pdb(filename).centroids()
	
	aux = []
	for i in range(len(sel.dbrefs)):
		seqlen = abs(sel.dbrefs[i].seqend - sel.dbrefs[i].seqbegin)
		
		dbrefsel = sel.resnum(list(range(sel.dbrefs[i].seqbegin, sel.dbrefs[i].seqend + 1)))
		g = distance_graph(dbrefsel, transform_fn=lambda x: x if x <=8 else None)
		diam = nx.algorithms.distance_measures.diameter(g)
		dac = nx.algorithms.assortativity.degree_assortativity_coefficient(g)
		clus = approximation.average_clustering(g)

		aux.append((seqlen, diam, dac, clus))
	
	return aux

def plot(filename):
	sel = read_pdb(filename).centroids()	
	dis = rel_distance_matrix(sel, sequence_min_neighborhood=1)
	plt.figure()
	plt.title(filename)
	plt.imshow(dis.todense())
	plt.savefig("{}.png".format(filename))
	plt.close()
	return dis

def png(filename):
	sel = read_pdb(filename).centroids()
	dis = rel_distance_matrix(sel, sequence_min_neighborhood=1)
	im = Image.fromarray(dis)
	im.save(filename[:-4] + ".jpeg")
	return None

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Extract diameter features of protein models')
	parser.add_argument("-m", "--models", nargs="+")
	parser.add_argument("-g", "--glob")
	parser.add_argument("-v", "--verbose", action="store_true", default=False)
	parser.add_argument("-o", "--output", default=None, help="output")
	parser.add_argument("-p", "--parallel", action="store_true", default=False)
	
	args = parser.parse_args()

	out = open(args.output, "w") if args.output else sys.stdout
	try:

		if args.glob:
			fs = GlobFileSet(args.glob)			
		elif args.models:
			fs = FileSet(args.models)

		ds, fails = fs.map(diameter, show_progress=True, parallelize=args.parallel, exceptions_to_collect=(ValueError, nx.exception.NetworkXError))
		print("Completed with {} success and {} fails".format(len(ds), len(fails)))
		
		if args.verbose:
			for f, e in fails.items():
				print(f, e)

		out.write("# Diameter Features Dataset\n")		
		out.write("# By {} at {}\n".format(pwd.getpwuid(os.getuid()).pw_name, datetime.datetime.now()))
		out.write("# name,dbref,seqlen,diameter,degree_assortativity_coefficient,average_clustering\n")
		
		for k, v in ds.items():	
			for i in range(len(v)):
				out.write("{},{},{:d},{:d},{:.2f},{:.2f}\n".format(k, i, *v[i]))
	finally:
		out.close()	