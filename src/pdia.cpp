#include <iostream>
#include <vector>
#include <cmath>
#include <climits>
#include <argparse.hpp>
#include <iobio.hpp>
#include <graph.hpp>

using namespace std;
using namespace iobio;
using namespace graph;

// graph
// d_max
// d_mean
// d_std
// d_2q
// d_3q

// d_xy
// d_yz
// d_zx

void print_atoms(const vector<PDBAtom> &atoms) {
	for (auto a: atoms) {
		cout << a.resname() << "-" << a.resnum() << a.name() << endl;
	}
}

void loop(const vector<PDBAtom> &atoms, int seqbegin = INT_MAX, int seqend = INT_MIN) {
	Graph cnet;
	int seqlen;
	int minres = INT_MAX;
	int maxres = INT_MIN;

	for (int i = 0, n = atoms.size(); i < n; i++) {		
		int resnum = atoms[i].resnum();
		if (resnum < seqbegin || resnum > seqend) {
			continue;
		}
		minres = resnum < minres ? resnum : minres;
		maxres = resnum > maxres ? resnum : maxres;
		for (int j = 0; j < i; j++) {
			if (atoms[j].resnum() < seqbegin || atoms[j].resnum() > seqend) {
				continue;
			}
			float dx = atoms[i].x() - atoms[j].x();
			float dy = atoms[i].y() - atoms[j].y();
			float dz = atoms[i].z() - atoms[j].z();

			float distance = sqrt(dx*dx + dy*dy + dz*dz);
			if (distance < 8.0) {
				cnet.link(i, j);
			}
		}
		seqlen = maxres - minres;
	}

	cout << " " << seqlen << " " << cnet.diameter();
}

void print(map<string, string> &m) {
	for (auto it: m) {
		cout << it.first << ": " << it.second << ", ";
	}
	cout << endl;
}

int main(int argc, char const *argv[])
{
	ArgumentParser parser;
	parser.add_argument("-c", "--cutoff").default_value("1");
	parser.add_argument("-k", "--min-distance").default_value("1");
	parser.add_argument("-v", "--verbose").default_value("0");
	parser.add_argument("-s", "--seqlen").default_value("0");
	parser.add_argument("input");

	//parser.usage();

	auto args = parser.parse_args(argc, argv);

	const string& filename = args["input"];
	try {
		ifstream ifs(filename);		
		if (!ifs.is_open()) {
			cerr << "Unable to open file \"" << filename << "\"" << endl;
			exit(0);
		}
		PDBFile pdb(&ifs);
		const vector<PDBAtom> atoms = pdb.centroids();

		const vector<PDBDbRef> dbrefs = pdb.dbrefs();
		int dbrefidx = 1;
		for (auto dbref: dbrefs) {
			cout << filename << dbrefidx++;
			loop(atoms, dbref.seqbegin(), dbref.seqend());
			cout << endl;
		}
		if (dbrefs.size() == 0) {		
			cout << filename << 1;
			loop(atoms);
			cout << endl;
		}
	} catch (const ifstream::failure &e) {
		cout << "Fail reading file" << endl;
		cout << "What: " << e.what() << endl;
	}

	return 0;
}